= HTTP Parameter Pollution

//tag::abstract[]

HTTP Parameter Pollution (HPP) happens when
the program makes no distinction
about the source of the request parameters or
accept duplicate parameters.
This can result in input validation bypass
such as bypassing authentication.

//end::abstract[]

This vulnerability is commonly identified in programming
language frameworks and causes the program
to interpret parameters in unanticipated ways.

//tag::lab[]

== Lab

Your objective in each lab is to pass the build.

=== Task 0

*Fork* and clone this repository.
Install `docker` and `make` on your system.

. Build the program: `make build`.
. Run it: `make run`.
. Run unit tests: `make test`. 
. Run security tests: `make securitytest`.

Note: The last test will fail. 

=== Task 1

Review the program code try to find out 
why security tests fails. 
Submit a sample request: 
`curl http://localhost:8080/?action=transfer&amount=100`

Note: Avoid looking at tests or patch file and try to
spot the vulnerable pattern on your own.

=== Task 2

The program is vulnerable to HPP.
Find how to patch this vulnerability.

=== Task 3

Review `test/appSecurity.test.js` and see how security tests
works. Review your patch from the previous task.
Make sure this time the security tests pass.
If you stuck, move to the next task.

=== Task 4

Check out the `patch` branch:

* Read link:cheatsheet.adoc[] and apply to your solution.
* Review the patched program.
* Run all tests.

=== Task 5

Push you code and make sure build is passed
Finally, send a pull request (PR).

//end::lab[]

//tag::references[]

== References

* OWASP Application Security Verification Standard 4.0, 5.1.1
* CWE 235
* https://github.com/expressjs/express/issues/1824[HTTP parameter pollution in Express can aid attackers in bypassing security filters.]

//end::references[]

include::CONTRIBUTING.adoc[]

== License

See link:LICENSE[]